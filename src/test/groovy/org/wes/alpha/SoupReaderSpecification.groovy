package org.wes.alpha

import org.wes.alpha.models.SearchBoard
import spock.lang.Specification


class SoupReaderSpecification extends Specification {
    def "Read a word with a space"() {
        given:
        SoupReader sr = new  SoupReader();
        ClassLoader classLoader = FullSpecification.class.getClassLoader();
        File input = new File(classLoader.getResource("1x2.txt").getFile());

        when:

        SearchBoard sb = sr.readFile(input)

        then:
        sb.getWords().get(0).equals("AB")
    }

}