package org.wes.alpha

import org.wes.alpha.models.SearchBoard
import spock.lang.Specification

import java.awt.Point


class AnswerKeyGeneratorSpecification extends Specification {
    def "Word not in SearchBoard"() {
        given:
            SearchBoard sb = new SearchBoard()
            sb.setSize(new Point(1,1))
            sb.setWords(["A"])
            def map = [(new Point(0,0)):"B"]
            sb.setWordMap(map)
            AnswerKeyGenerator akg = new AnswerKeyGenerator();
            akg.setSearchboard(sb)
        when:
            akg.findWords()

        then:
            sb.key.size() == 0
    }
    def "Word longer than  the grid"() {
        given:
        SearchBoard sb = new SearchBoard()
        sb.setSize(new Point(1,1))
        sb.setWords(["AB"])
        def map = [(new Point(0,0)):"B"]
        sb.setWordMap(map)
        AnswerKeyGenerator akg = new AnswerKeyGenerator();
        akg.setSearchboard(sb)
        when:
        akg.findWords()

        then:
        sb.key.size() == 0
    }
    def "Unicode in SearchBoard"(){
        given:
        SearchBoard sb = new SearchBoard()
        sb.setSize(new Point(1,1))
        sb.setWords(["☠"])
        def map = [(new Point(0,0)):"☠"]
        sb.setWordMap(map)
        AnswerKeyGenerator akg = new AnswerKeyGenerator();
        akg.setSearchboard(sb)

        when:
        akg.findWords()

        then:
        sb.key.size() == 1
        sb.key.get("☠").getStart() == sb.key.get("☠").getFinish()
        sb.key.get("☠").getStart().equals(new Point(0,0))
    }
    def "Word in SearchBoard"() {
        given:
        SearchBoard sb = new SearchBoard()
        sb.setSize(new Point(1,1))
        sb.setWords(["A"])
        def map = [(new Point(0,0)):"A"]
        sb.setWordMap(map)
        AnswerKeyGenerator akg = new AnswerKeyGenerator();
        akg.setSearchboard(sb)

        when:
        akg.findWords()

        then:
        sb.key.size() == 1
        sb.key.get("A").getStart() == sb.key.get("A").getFinish()
        sb.key.get("A").getStart().equals(new Point(0,0))
    }
}