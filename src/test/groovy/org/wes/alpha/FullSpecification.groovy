package org.wes.alpha

import org.wes.alpha.models.SearchBoard
import org.wes.alpha.models.WordLocation
import spock.lang.Specification

import java.awt.Point


class FullSpecification extends Specification {
    def "Given Test 1"() {
        given:
        SoupReader sr = new  SoupReader();
        AnswerKeyGenerator keygen = new AnswerKeyGenerator()
        SearchBoard sb
        WordLocation answerABC, answerAEI


        when:
        ClassLoader classLoader = FullSpecification.class.getClassLoader();
        File input = new File(classLoader.getResource("3x3.txt").getFile());
        sb = sr.readFile(input)
        keygen.setSearchboard(sb)
        keygen.findWords()

        then:
        sb.key.get("ABC").getStart() == new Point(0,0)
        sb.key.get("ABC").getFinish() == new Point(2,0)
        sb.key.get("AEI").getStart() == new Point(0,0)
        sb.key.get("AEI").getFinish() == new Point(2,2)
    }
    def "Given Test 2"() {
        given:
        SoupReader sr = new  SoupReader();
        AnswerKeyGenerator keygen = new AnswerKeyGenerator()
        SearchBoard sb
        WordLocation answerHELLO, answerGOOD, answerBYE
        ClassLoader classLoader = FullSpecification.class.getClassLoader();
        File input = new File(classLoader.getResource("5x5.txt").getFile());


        when:
        sb = sr.readFile(input)
        keygen.setSearchboard(sb)
        keygen.findWords()

        then:
        sb.key.get("HELLO").getStart() == new Point(0,0)
        sb.key.get("HELLO").getFinish() == new Point(4,4)
        sb.key.get("GOOD").getStart() == new Point(0,4)
        sb.key.get("GOOD").getFinish() == new Point(3,4)
        sb.key.get("BYE").getStart() == new Point(3,1)
        sb.key.get("BYE").getFinish() == new Point(1,1)
    }
    def "Reverse word with a space"() {
        given:
        SoupReader sr = new  SoupReader();
        AnswerKeyGenerator keygen = new AnswerKeyGenerator()
        SearchBoard sb
        WordLocation answer
        ClassLoader classLoader = FullSpecification.class.getClassLoader();
        File input = new File(classLoader.getResource("1x2b.txt").getFile());


        when:
        sb = sr.readFile(input)
        keygen.setSearchboard(sb)
        keygen.findWords()

        then:
        sb.key.get("AB").getStart() == new Point(2,0)
        sb.key.get("AB").getFinish() == new Point(1,0)
    }
    def "Rectangle"() {
        given:
        SoupReader sr = new  SoupReader();
        AnswerKeyGenerator keygen = new AnswerKeyGenerator()
        SearchBoard sb
        WordLocation answer


        when:
        ClassLoader classLoader = FullSpecification.class.getClassLoader();
        File input = new File(classLoader.getResource("1x3.txt").getFile());
        sb = sr.readFile(input)
        keygen.setSearchboard(sb)
        keygen.findWords()

        then:
        sb.key.get("C").getStart() == new Point(2,0)
        sb.key.get("C").getFinish() == new Point(2,0)
    }

}