package org.wes.alpha;

import org.wes.alpha.models.SearchBoard;

import java.awt.*;
import java.io.File;
import java.io.FileNotFoundException;
import java.util.Scanner;

/**
 * This class is used to generate the SearchBoard from an input file
 */
public class SoupReader {

    /**
     * This function Reads a file from input and generates the SearchBoard
     * @param input the location of the input file
     * @return Searchboard caluclated from the file
     * @throws FileNotFoundException
     */
    public SearchBoard readFile(File input) throws FileNotFoundException {

        Scanner scanner = new Scanner(input);
        SearchBoard searchboard = new SearchBoard();
        Point size = getcoordinates(scanner.nextLine());
        searchboard.setSize(size);
        //get Search Grid letters
        for (int row = 0; row < searchboard.getSize().y; row++) {
            for (int col = 0; col < searchboard.getSize().x; col++) {
                searchboard.getWordMap().put(new Point(col, row), scanner.next().toCharArray()[0]);
            }
        }
        //get the end of the grid
        scanner.nextLine();
        //get words to find
        while(scanner.hasNextLine())
        {
            searchboard.getWords().add(scanner.nextLine().replace(" ",""));
        }
        return searchboard;
    }

    /**
     *  Takes in coordinates with 'x' as the delimiter and parses out the row and cols
     * @param coordinates
     * @return  A Point of the largets coordinate in the searchboard
     */
    private static Point getcoordinates(String coordinates) {
        //The size of the grid comes is provide by row X cols
        String[] coordinate = coordinates.split("x");
        return new Point(Integer.parseInt(coordinate[1]), Integer.parseInt(coordinate[0]));
    }

}