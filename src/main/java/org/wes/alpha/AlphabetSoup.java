package org.wes.alpha;

import org.wes.alpha.models.SearchBoard;

import java.io.File;
import java.io.FileNotFoundException;

/**
 * Alphabet Soup
 */
public class AlphabetSoup {
    public static void main(String[] args) {
        SoupReader sr = new SoupReader();
        try {
            ClassLoader classLoader = AlphabetSoup.class.getClassLoader();
            //assuming the file exists
            File input = new File(args[0]);
            SearchBoard searchBoard = sr.readFile(input);
            AnswerKeyGenerator keygen = new AnswerKeyGenerator();
            keygen.setSearchboard(searchBoard);
            keygen.findWords();
            keygen.getSearchboard().getKey().forEach((word, wordLocation) -> System.out.println(
                    wordLocation.outputFormat()));
        } catch (FileNotFoundException e) {
            System.out.println(e.toString());
        }
    }
}
