package org.wes.alpha;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.wes.alpha.models.SearchBoard;
import org.wes.alpha.models.WordLocation;

import java.awt.*;
import java.util.HashMap;

/**
 * This class is used to generate an answer key for the search board
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class AnswerKeyGenerator {
    private SearchBoard searchboard;

    /**
     * A function to find the start and finish of all the words in a searchboard
     * The final location get updated in the searchboard if words are found
     */
    public void findWords() {
        for (int row = 0; row < searchboard.getSize().y; row++) {
            for (int col = 0; col < searchboard.getSize().x; col++) {
                for (int searchListIndex = 0; searchListIndex < searchboard.getWords().size(); searchListIndex++) {
                    Point startMark = new Point(col,row);
                    String searchTerm = searchboard.getWords().get(searchListIndex);
                    search(startMark, searchTerm);
                }
            }
        }
    }

    /**
     * A utility function to look for a word in every direction at a certain point in a search Area for a particular word
     * The final location get updated in the searchboard if a word is found
     * @param start  The corrdinate to find the word
     * @param word  The word to be looked for
     */
    public void search(Point start, String word) {
        HashMap<Point, Character> searchgrid = searchboard.getWordMap();
        Point marker = start;
        Integer direction []= {1,0,-1};//For positive or negative directions in x and y
        for(int yOptions =0; yOptions < direction.length;yOptions++) {

            for (int xOptions = 0; xOptions < direction.length; xOptions++) {
                StringBuilder sb = new StringBuilder("");
                for (int i = 0; i < word.length(); i++) {
                    marker = new Point(start.x + (i * direction[xOptions]), start.y+ (i * direction[yOptions]));
                    if (searchgrid.containsKey(marker)) {
                        sb.append(searchgrid.get(marker));
                    }
                }
                WordLocation answer = new WordLocation(start, marker, word);
                if (sb.toString().equals(word)&& !searchboard.getKey().containsKey(word)) {
                    searchboard.getKey().put(word,answer);
                }

            }
        }
    }



}
