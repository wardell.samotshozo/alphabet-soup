package org.wes.alpha.models;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.awt.*;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class WordLocation {
    private Point start;
    private Point finish;
    private String word;
    public String  outputFormat(){
        return word + " " + formatPoint(start) + " " + formatPoint(finish);
    }
    public String formatPoint(Point mark){
        return mark.y +":" +mark.x;
    }
}
