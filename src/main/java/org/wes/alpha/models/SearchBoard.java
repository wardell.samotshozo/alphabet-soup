package org.wes.alpha.models;

import java.awt.*;
import java.util.ArrayList;
import java.util.HashMap;

import lombok.Data;
import lombok.AllArgsConstructor;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class SearchBoard {
    private Point size;
    private ArrayList<String> words = new ArrayList<String>();
    private HashMap <Point,Character> WordMap = new HashMap<Point,Character>();
    private HashMap <String, WordLocation> key = new HashMap<String,WordLocation>();
}
